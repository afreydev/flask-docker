FROM ubuntu:latest
MAINTAINER Fernando Rey "afreydev@gmail.com"
RUN apt-get update -y
RUN apt-get install -y python3 python3-pip
WORKDIR /usr/bin
RUN ln -s python3 python
RUN ln -s pip3 pip
RUN pip install Flask
COPY . /app
WORKDIR /app
ENTRYPOINT ["python"]
CMD ["metodos.py"]
